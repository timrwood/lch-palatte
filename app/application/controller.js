import Ember from 'ember';

const { computed } = Ember;

export default Ember.Controller.extend({
  queryParams: ['h', 's', 'l', 'shift', 'saturationShift'],

  x: 0,
  y: 0,

  h: '',
  s: '',
  l: '',

  shift: 0,
  saturationShift: 0,

  hs: computed('h', function () {
    return this.get('h').split('_').map(k => k | 0);
  }),
  ls: computed('l', function () {
    return this.get('l').split('_').map(k => k | 0);
  }),

  changeLightness(v) {
    let y = this.get('y');
    let ls = this.get('ls').slice();
    ls[y] = Math.max(Math.min(ls[y] + v, 100), 0);
    this.set('l', ls.join('_'));
  },

  changeSaturation(v) {
    let s = (this.get('s') | 0) + v;
    this.set('s', Math.max(Math.min(s, 100), 0));
  },

  changeShift(v) {
    let s = (this.get('shift') | 0) + v;
    this.set('shift', Math.max(Math.min(s, 360), -360));
  },

  changeSaturationShift(v) {
    let s = (this.get('saturationShift') | 0) + v;
    this.set('saturationShift', Math.max(Math.min(s, 100), -100));
  },

  changeHue(v) {
    let x = this.get('x');
    let hs = this.get('hs').slice();
    hs[x] = (hs[x] + 360 + v) % 360;
    this.set('h', hs.join('_'));
  },

  showingHelp: true,

  actions: {
    setCell(x, y) {
      this.set('x', x);
      this.set('y', y);
    }
  }
});
