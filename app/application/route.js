import Ember from 'ember';
import { MousetrapRoute, mousetrap } from 'ember-mousetrap';

export default Ember.Route.extend(MousetrapRoute, {
  setupController(controller) {
    Ember.run.next(() => {
      if (controller.get('h') === '') {
        controller.set('h', '10_45_95_160_230');
      }
      if (controller.get('l') === '') {
        controller.set('l', '80_60_50_40_20');
      }
      if (controller.get('s') === '') {
        controller.set('s', 70);
      }
    });
  },

  shortcuts: {
    up1: mousetrap('up', function () {
      this.controller.changeLightness(1);
    }),
    up10: mousetrap('shift+up', function () {
      this.controller.changeLightness(10);
    }),

    down1: mousetrap('down', function () {
      this.controller.changeLightness(-1);
    }),
    down10: mousetrap('shift+down', function () {
      this.controller.changeLightness(-10);
    }),

    left1: mousetrap('left', function () {
      this.controller.changeHue(-1);
    }),
    left10: mousetrap('shift+left', function () {
      this.controller.changeHue(-10);
    }),

    right1: mousetrap('right', function () {
      this.controller.changeHue(1);
    }),
    right10: mousetrap('shift+right', function () {
      this.controller.changeHue(10);
    }),

    saturate1: mousetrap(']', function () {
      this.controller.changeSaturation(1);
    }),
    saturate10: mousetrap('shift+]', function () {
      this.controller.changeSaturation(10);
    }),

    desaturate1: mousetrap('[', function () {
      this.controller.changeSaturation(-1);
    }),
    desaturate10: mousetrap('shift+[', function () {
      this.controller.changeSaturation(-10);
    }),

    shift1: mousetrap('.', function () {
      this.controller.changeShift(1);
    }),
    shift10: mousetrap('shift+.', function () {
      this.controller.changeShift(10);
    }),

    unshift1: mousetrap(',', function () {
      this.controller.changeShift(-1);
    }),
    unshift10: mousetrap('shift+,', function () {
      this.controller.changeShift(-10);
    }),

    saturateShift1: mousetrap('0', function () {
      this.controller.changeSaturationShift(1);
    }),
    saturateShift10: mousetrap('shift+0', function () {
      this.controller.changeSaturationShift(10);
    }),

    saturateUnshift1: mousetrap('9', function () {
      this.controller.changeSaturationShift(-1);
    }),
    saturateUnshift10: mousetrap('shift+9', function () {
      this.controller.changeSaturationShift(-10);
    })
  },
});
