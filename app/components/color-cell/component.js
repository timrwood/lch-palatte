import Ember from 'ember';
import { toHex } from 'husl-palette/utils/husl';

const { computed } = Ember;

export default Ember.Component.extend({
  classNames: 'row',
  attributeBindings: 'style',

  hue: 0,
  lightness: 50,
  saturation: 50,
  shift: 0,
  saturationShift: 0,

  hsl: computed('hue', 'lightness', 'saturation', 'shift', 'saturationShift', function () {
    let { hue, lightness, saturation, shift, saturationShift } = this.getProperties('hue', 'lightness', 'saturation', 'shift', 'saturationShift');
    hue |= 0;
    lightness |= 0;
    saturation |= 0;
    saturationShift |= 0;
    shift |= 0;
    hue += Math.round(((lightness - 50) / 50) * shift);
    hue = ((hue % 360) + 360) % 360;
    saturation += Math.round(((lightness - 50) / 50) * saturationShift);
    saturation = Math.max(0, Math.min(100, saturation));
    return { hue, lightness, saturation };
  }),

  hex: computed('hsl', function () {
    let { hue, lightness, saturation } = this.get('hsl');
    return toHex(hue, saturation, lightness);
  }),

  textColor: computed('hue', 'lightness', 'saturation', function () {
    let { hue, lightness, saturation } = this.get('hsl');
    lightness += lightness < 50 ? 10 : -10;
    return toHex(hue, saturation, lightness);
  }),

  style: computed('hex', 'textColor', function () {
    return `background:${this.get('hex')};color:${this.get('textColor')}`.htmlSafe();
  }),

  mouseEnter() {
    this.sendAction();
  }
});
