const m = {
  R: [3.2409699419045214, -1.5373831775700935, -0.49861076029300328],
  G: [-0.96924363628087983, 1.8759675015077207, 0.041555057407175613],
  B: [0.055630079696993609, -0.20397695888897657, 1.0569715142428786]
};

const m_inv = {
  X: [0.41239079926595948, 0.35758433938387796, 0.18048078840183429],
  Y: [0.21263900587151036, 0.71516867876775593, 0.072192315360733715],
  Z: [0.019330818715591851, 0.11919477979462599, 0.95053215224966058]
};

const refU = 0.19783000664283681;
const refV = 0.468319994938791;
const kappa = 903.2962962962963;
const epsilon = 0.0088564516790356308;

function getBounds(L) {
  let sub1 = Math.pow(L + 16, 3) / 1560896;
  let sub2 = sub1 > epsilon ? sub1 : L / kappa;
  let ret = [];
  let channels = ['R', 'G', 'B'];
  for (let i = 0; i < 3; i++) {
    let [m1, m2, m3] = m[channels[i]];
    for (let t = 0; t < 2; t++) {
      let top1 = (284517 * m1 - 94839 * m3) * sub2;
      let top2 = (838422 * m3 + 769860 * m2 + 731718 * m1) * L * sub2 - 769860 * t * L;
      let bottom = (632260 * m3 - 126452 * m2) * sub2 + 126452 * t;
      ret.push([top1 / bottom, top2 / bottom]);
    }
  }
  return ret;
}

function intersectLineLine(line1, line2) {
  return (line1[1] - line2[1]) / (line2[0] - line1[0]);
}

function distanceFromPole(point) {
  return Math.sqrt(Math.pow(point[0], 2) + Math.pow(point[1], 2));
}

function lengthOfRayUntilIntersect(theta, line) {
  let [m1, b1] = line;
  let len = b1 / (Math.sin(theta) - m1 * Math.cos(theta));
  if (len < 0) {
    return null;
  }
  return len;
}

function maxSafeChromaForL(L) {
  let lengths = getBounds(L).map(bounds => {
    let [m1, b1] = bounds;
    let x = intersectLineLine([m1, b1], [-1 / m1, 0]);
    return distanceFromPole([x, b1 + x * m1]);
  });
  return Math.min(...lengths);
}

function maxChromaForLH(L, H) {
  let hrad = H / 360 * Math.PI * 2;
  let lengths = getBounds(L).reduce((arr, line) => {
    let l = lengthOfRayUntilIntersect(hrad, line);
    if (l !== null) {
      arr.push(l);
    }
    return arr;
  }, []);
  return Math.min(...lengths);
}

function dotProduct(a, b) {
  var i, j, ref, ret;
  ret = 0;
  for (i = j = 0, ref = a.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
    ret += a[i] * b[i];
  }
  return ret;
}

function fromLinear(c) {
  if (c <= 0.0031308) {
    return 12.92 * c;
  } else {
    return 1.055 * Math.pow(c, 1 / 2.4) - 0.055;
  }
}

function toLinear(c) {
  let a = 0.055;
  if (c > 0.04045) {
    return Math.pow((c + a) / (1 + a), 2.4);
  } else {
    return c / 12.92;
  }
}

function xyz_to_rgb(tuple) {
  let R = fromLinear(dotProduct(m.R, tuple));
  let G = fromLinear(dotProduct(m.G, tuple));
  let B = fromLinear(dotProduct(m.B, tuple));
  return [R, G, B];
}

function rgb_to_xyz([R, G, B]) {
  let rgbl = [toLinear(R), toLinear(G), toLinear(B)];
  let X = dotProduct(m_inv.X, rgbl);
  let Y = dotProduct(m_inv.Y, rgbl);
  let Z = dotProduct(m_inv.Z, rgbl);
  return [X, Y, Z];
}

function y_to_L(Y) {
  if (Y <= epsilon) {
    return Y * kappa;
  } else {
    return 116 * Math.pow(Y, 1 / 3) - 16;
  }
}

function l_to_Y(L) {
  if (L <= 8) {
    return L / kappa;
  } else {
    return Math.pow((L + 16) / 116, 3);
  }
}

function xyz_to_luv([X, Y, Z]) {
  if (Y === 0) {
    return [0, 0, 0];
  }
  let L = y_to_L(Y);
  let varU = (4 * X) / (X + (15 * Y) + (3 * Z));
  let varV = (9 * Y) / (X + (15 * Y) + (3 * Z));
  let U = 13 * L * (varU - refU);
  let V = 13 * L * (varV - refV);
  return [L, U, V];
}

function luv_to_xyz([L, U, V]) {
  if (L === 0) {
    return [0, 0, 0];
  }
  let varU = U / (13 * L) + refU;
  let varV = V / (13 * L) + refV;
  let Y = l_to_Y(L);
  let X = 0 - (9 * Y * varU) / ((varU - 4) * varV - varU * varV);
  let Z = (9 * Y - (15 * varV * Y) - (varV * X)) / (3 * varV);
  return [X, Y, Z];
}

function luv_to_lch([L, U, V]) {
  let C = Math.sqrt(Math.pow(U, 2) + Math.pow(V, 2));
  let H = 0;
  if (C >= 0.00000001) {
    let Hrad = Math.atan2(V, U);
    H = Hrad * 360 / 2 / Math.PI;
    if (H < 0) {
      H = 360 + H;
    }
  }
  return [L, C, H];
}

function lch_to_luv([L, C, H]) {
  let Hrad = H / 360 * 2 * Math.PI;
  let U = Math.cos(Hrad) * C;
  let V = Math.sin(Hrad) * C;
  return [L, U, V];
}

function husl_to_lch([H, S, L]) {
  let C = 0;
  if (L <= 99.9999999 && L >= 0.00000001) {
    C = maxChromaForLH(L, H) / 100 * S;
  }
  return [L, C, H];
}

function lch_to_husl([L, C, H]) {
  let S = 0;
  if (L <= 99.9999999 && L >= 0.00000001) {
    S = C / maxChromaForLH(L, H) * 100;
  }
  return [H, S, L];
}

function huslp_to_lch([H, S, L]) {
  let C = 0;
  if (L <= 99.9999999 && L >= 0.00000001) {
    C = maxSafeChromaForL(L) / 100 * S;
  }
  return [L, C, H];
}

function lch_to_huslp([L, C, H]) {
  let S = 0;
  if (L <= 99.9999999 && L >= 0.00000001) {
    S = C / maxSafeChromaForL(L) * 100;
  }
  return [H, S, L];
}

function rgb_to_hex(tuple) {
  return "#" + tuple.map(ch => {
    ch = Math.round(ch * 1e6) / 1e6;
    ch = Math.min(1, Math.max(0, ch));
    ch = Math.round(ch * 255).toString(16);
    if (ch.length === 1) {
      ch = "0" + ch;
    }
    return ch;
  }).join('');
}

function hex_to_rgb(hex) {
  if (hex.charAt(0) === "#") {
    hex = hex.substring(1, 7);
  }
  return [
    hex.substring(0, 2),
    hex.substring(2, 4),
    hex.substring(4, 6)
  ].map(ch => parseInt(ch, 16) / 255);
}

function lch_to_rgb(tuple) {
  return xyz_to_rgb(luv_to_xyz(lch_to_luv(tuple)));
}

function rgb_to_lch(tuple) {
  return luv_to_lch(xyz_to_luv(rgb_to_xyz(tuple)));
}

function husl_to_rgb(tuple) {
  return lch_to_rgb(husl_to_lch(tuple));
}

function rgb_to_husl(tuple) {
  return lch_to_husl(rgb_to_lch(tuple));
}

export function fromRGB (R, G, B) {
  return rgb_to_husl([R, G, B]);
}

export function fromHex (hex) {
  return rgb_to_husl(hex_to_rgb(hex));
}

export function toRGB (H, S, L) {
  return husl_to_rgb([H, S, L]);
}

export function toHex (H, S, L) {
  return rgb_to_hex(husl_to_rgb([H, S, L]));
}

export function toRGBP (H, S, L) {
  return xyz_to_rgb(luv_to_xyz(lch_to_luv(huslp_to_lch([H, S, L]))));
}

export function toHexP (H, S, L) {
  return rgb_to_hex(xyz_to_rgb(luv_to_xyz(lch_to_luv(huslp_to_lch([H, S, L])))));
}

export function fromRGBP (R, G, B) {
  return lch_to_huslp(luv_to_lch(xyz_to_luv(rgb_to_xyz([R, G, B]))));
}

export function fromHexP (hex) {
  return lch_to_huslp(luv_to_lch(xyz_to_luv(rgb_to_xyz(hex_to_rgb(hex)))));
}
